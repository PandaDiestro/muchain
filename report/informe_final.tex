\documentclass[10pt,a4paper,journal,twocolumn]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage[]{amsthm}
\usepackage[]{amssymb}

\usepackage{mathtools}
\usepackage{minted}

\usepackage{graphicx}
\graphicspath{ {./media/} }

\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

\title{
    \textmu chain - Modelo de sintetizador de melodías pseudo-aleatorias basado en Árboles

    \vspace{-0.1cm}
    \author{\footnotesize Proyecto final de matemática discreta, Rodrigo José Alva Sáenz}
    \date{}
}

\providecommand{\keywords}[1]
{
    \small
    \textbf{\textit{Keywords---}} #1
}

\providecommand{\nabstract}[1]
{
    \small
    \textbf{\textit{Abstract---}} #1
}

\renewcommand{\thesection}{\normalsize\normalfont\Roman{section}.}
\renewcommand{\thesubsection}{\normalsize\normalfont\it\Alph{subsection}.}
\renewcommand{\thesubsubsection}{\small\arabic{subsubsection}.}

\begin{document}

\maketitle %This command prints the title based on information entered above

\nabstract{Hello}

\bigskip

\keywords{Teoría de grafos, Procesamiento de señales digitales, Árboles}

\bigskip

\section{\normalsize\normalfont\centering Introducción}

El efecto que tiene el sonido, más específicamente la música, en aspectos de nuestro estado psicológico, es un fenómeno que empezó a ser estudiado desde hace poco. La mayoría de estos suele concluir en que la naturaleza de la estructura de esta música no es la que influencia o afecta la personalidad de los oyentes en sí, sino el apego personal que cada uno tiene con el sonido determinado en sí.


Esta noción se ve reforzada por estudios como el de Powell et. al. (2022) \cite{Powell2022-xe}, donde se consigue relacionar a la "música violenta" con un factor más ligado a la curiosidad que a actitudes negativas. Esto en contraposición con el la imagen usual que se le suele dar a determinados tipos de música al relacionarlos con actitudes determinadas. Esto fue determinado estudiando justamente ambos factores (factor de curiosidad y géneros de música preferidos) en el grupo de participantes. Es entonces que se puede reconocer que la música tiene un valor agregado extrínseco a su propia "naturaleza" \footnote{La "naturaleza" de la música se refiere a la forma en la que se usa su género para ser clasificada, en el caso del estudio, por ejemplo, en "violenta" y "no violenta".}.

La música también puede ser utilizada como estímulo positivo en los escenarios donde se necesite. Las personas suelen escuchar familias determinadas de géneros musicales en una plétora de distintos escenarios, ya sean positivos o negativos, es así que, como ocurre con cualquier otro patrón de comportamiento o ocurrencia, nuestros cerebros generan una relación mayor entre esta música y la forma en la que esos escenarios se dieron. En estudios como el que realizaron de Cadwell et. al. (2022) \cite{Lichtl2022-wt} se aborda justamente una de las maneras en las que se puede usar la música como estímulo positivo para contrarrestar los efectos de otro negativo. En el caso oncológico del estudio se trata al dolor como principal punto de inflexión. La música se usa como parte del tratamiento de dolor que se suministra a los pacientes para que el tratamiento que se les está dando sea más "llevadero".

La música, a parte de ser implementada de forma directa para mejorar algún aspecto del paciente, también puede ser utilizada como complemento para sistemas psico-terapeutas más complejos, esto gracias a que, como explican Heping y Bin (2022) \cite{Heping2022-fn}, la correlación que existe entre habilidades cognitivas como la aptitud en lenguas, la concentración, etc. con la forma en la que percibimos música. La musica ha demostrado también funcionar como factor de protección al deterioro mental ocasinado por la edad (Schneider, Hunter \& Bardach, 2019) \cite{Schneider2019-zw}, por ejemplo, al reforzar los procesos de retribución de memoria en adultos mayores (Bugos, 2010) \cite{Bugos2010TheBO}. En el caso de la investigación de Heping y Bin (2022) \cite{Heping2022-fn}, la música era utilizada como un objeto más dentro de la cadena de tratamiento de pacientes que sufrían depresión, el paciente escuchaba una muestra de música generada a tiempo real a modo de retroalimentación, mientras el sistema le hacía preguntas para determinar su estado y poder encontrar un tratamiento adecuado, de ser necesario. Al final, se demostró que el sistema que usaba retroalimentación musical era bastante efectivo.

Se puede reconocer el potencial de la música personalizada en el mejoramiento del estado psicológico de las personas, así como también el potencial de los proyectos basados en los conceptos de la música generativa para este mismo fin. El presente proyecto tuvo como objetivo desarrollar un sistema que, no sólo sea capaz de sintetizar muchos tipos distintos de ondas a la vez, sino que permita al usuario definir melodías a su propio gusto, de este modo tienen un mayor apego a lo que están escuchando. Además, el enfoque del estudio plantea definir melodías como n-árboles, donde por cada vez que se produce una melodía esta es una rama aleatoria distinta del árbol, lo que reduce la monotonía de lo que el usuario escucha. Se plantea, ademá, que el conjunto de todas las melodías aleatorias, luego, puede ser considerado una máquina de estados, donde cada estado es una nota distinta que el sintetizador reproducirá.

El procesamiento de señales digitales es una de las ramas más importantes de la matemática discreta, es la forma \textit{de-facto} por la que se analiza la naturaleza de las señales analógicas del ambiente en dominios discretos mediante la generación o toma de muestras. Este proyecto, al requerir basarse en la sintetización digital de muestras de audio, aplica conceptos propios de este campo de estudio para poder producir muestras de ondas de audio personalizadas.

Este proyecto adquiere valor pues, hoy en día, los ambientes en los que nos desarrollamos como individuos suelen no ser los más adecuados para el estado mental. Existen medidores cuya naturaleza insostenible y diferencia notable con etapas anteriores demuestran una naturaleza preocupante, tanto los niveles de estrés cotidiano, como las tasas de suicidio, las tasas de depresión en personas, etc. son sólo algunos de estos ejemplos. Se requieren sistemas especializados y personalizables que permitan mejorar el estado psicológico de sus usuarios, es ahí donde el proyecto de este reporte entra a intervenir, pues planteamos un sistema de apoyo para el usuario en un estado psicológico bajo.

\section{\normalsize\normalfont\centering Marco teórico}

\subsection{\normalsize\normalfont\it Señal de audio}

Una señal de audio puede verse desde 2 puntos en los que existe. Por un lado es una vibración que se propaga en el aire y luego es procesada por nuestros oídos. Sin embargo, para que una computadora, que trabaja sobre conjuntos digitales o discretos, pueda procesar este audio del mismo modo que nosotros, se tiene que de algún modo convertir el rango naturalmente analógico de esta señal a uno digital. Para esto se usa la toma de muestras de audio en puntos precisos de tiempo. Es por ello que toda muestra de audio está ligada a un valor llamado "Sample Rate" o "Tasa de muestreo", esta representa, por segundo, el número de muestras que existen. De modo que mientras más alta sea la tasa de muestro la calidad final también lo será y por lo tanto su cercanía con la muestra analógica original.

En \textmu chain se tuvo que aplicar este mismo concepto pero al revés, la esencia de la sintetización de audio está en generar valores o "muestras" de amplitud bajo dominios discretos para acercarse a cómo sonaría el audio original, de ser este un sonido analógico real. Es decir, seguimos usando las funciones de onda de cada oscilador del sintetizador, pero tomamos un número finito de puntos y no una sola función continua.

\subsection{\normalsize\normalfont\it Grafos}

La estructura interna de \textmu chain se basa de forma fuerte en la teoría de grafos, pero más específicamente en 2 subtipos: \textbf{N-Árboles} y \textbf{Máquinas de estado}.

\subsubsection{\small N-Árboles}

Cada sintetizador dentro de \textmu chain es una estrutura que contiene "procesadores de muestras" similares a simplemente funciones.

\begin{center}
    \includegraphics[scale=0.4]{sampleProc.png}
\end{center}

Para que cualquier otra estructura sea también parte de el "sintetizador", esta debe cumplir 1 sola regla, implementar la interfaz "SignalProc" que a su vez tiene 1 sólo método, el que se encargará de calcular el valor de amplitud en tiempo para ese único nodo.

\bigskip

\begin{figure}[htp]
    \centering
    \begin{minted}[fontsize=\scriptsize]{go}
type SignalProc interface {
    Process(float64, float64, float64)  float64
}
    \end{minted}
\end{figure}


\bigskip

Por ejemplo, el oscilador sinusoidal tiene la siguiente estructura:

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
type SINE struct {
    Volume  float64
}

func (proc *SINE) Process(timeStamp float64,
    frequency float64,
    amplitude float64) float64 {
    var angle float64 = timeStamp * frequency
    signal := amplitude * math.Sin(2 * math.Pi * angle)
    return signal * proc.Volume * 0.01
}
\end{minted}

\bigskip

Podemos notar que como la interfaz "SignalProc" es totalmente genérica entonces podríamos definir un oscilador como otro subconjunto de osciladores que implementen la misma función y asi sucesivamente.

La naturaleza de esta estructura otorfa al sintetizador de una mucho mayor "modularidad", donde las estructuras que se usen para hacer sonidos pueden ser de muy simples como una sola onda a muy complejas como un sintetizador de órgano de 8 osciladores.


\subsubsection{\small Máquina de estados finitos (FSM)}

Como se mencionó anteriormente, una melodía puede tomarse como una máquina de estados, donde cada estado es una nota y las transiciones definen el ritmo de la secuencia.

En este caso, bajo ese concepto, se tomó el grupo de todas las unidades de melodía como una gran máquina de estados, definida por nodos y conexiones que apunten una a otra, considerando además que únicamente puede contener la información de 1 estado a la vez (la nota musical, en este caso).

\subsection{\normalsize\normalfont\it Recursividad}

Como se mostró anteriormente, todos los osciladores son, en esencia, implementaciones distintas para una misma \textit{interfaz}, "SignalProc". A partir de ello podemos ver la naturaleza de el sintetizador grande, siendo este otra implementación de la interfaz mencionada, con la diferencia de que dentro de sí contiene una lista de tamaño indefinido de otros procesadores más (por ello se llama "n-oscilador").

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
type NxOsc struct {
    Oscillators     []SignalProc
    Channels        uint16
}

func (proc nxosc) process(...) float64 {
    var signal float64
    for _, osc := range proc.oscillators {
        signal += osc.process(...)
    }

    return (signal / float64(len(proc.oscillators))) /
            float64(proc.channels)
}
\end{minted}

\bigskip

La recursividad permitió al proyecto ser mucho más flexible con la capacidad que tiene para sintetizar muestras de audio, sin ella, la estructura tendría que ser inmutable o sobresimplificada, lo que no hubiera permitido a los usuarios diseñar a cabalidad sus propios sonidos.

\section{\normalsize\normalfont\centering Metodología}

Se llevará a cabo un enfoque matemático cuantitativo para este estudio. Dado que la recopilación de datos no es posible mediante encuestas o información directa de individuos reales, se utilizarán datos simulados basados en perfiles hipotéticos de personas. Estos datos simulados se generarán de acuerdo con patrones y características que podrían representar a individuos reales en el contexto de interés, basando estos mismos patrones de la literatura expuesta previamente.

\subsection{\normalsize\normalfont\it Diseño del estudio}

El diseño del estudio se basará en la creación de perfiles simulados de individuos, los cuales se utilizarán para generar datos ficticios que representen características y comportamientos de posibles personas reales. Estos perfiles serán creados considerando diversas variables relevantes para el estudio, como edad, género, preferencias musicales, nivel socioeconómico, entre otros. La selección de casos se llevará a cabo mediante la configuración de estos perfiles simulados, buscando cubrir una amplia gama de escenarios hipotéticos.

Las herramientas y técnicas a utilizar implicarán el uso de software de generación de datos sintéticos y la aplicación de algoritmos y modelos matemáticos para establecer correlaciones y patrones entre las variables relevantes. También se emplearán herramientas estadísticas para la validación y análisis de los datos generados.

\subsection{\normalsize\normalfont\it Recopilación y análisis de datos}

Los datos se recopilarán mediante la generación de perfiles simulados, creando registros ficticios que representen características demográficas, psicológicas y de comportamiento de posibles individuos. Estos "individuos simulativos" serán sometidos a experimentación, luego, se aplicarán técnicas de análisis de datos descriptivos e inferenciales para identificar tendencias, correlaciones y posibles relaciones entre los componentes del estudio.

El análisis se centrará en la exploración de patrones y la generación de conclusiones basadas en la simulación de datos realistas. La interpretación de estos datos simulados proporcionará una comprensión teórica y proyectiva de posibles comportamientos y tendencias en un escenario hipotético.

La rigurosidad del estudio se mantendrá a través de la validez de los modelos simulados utilizados, así como la coherencia y consistencia en la generación de datos simulados basados en perfiles de posibles individuos.

\section{\normalsize\normalfont\centering Desarrollo e implementación}

Antes de iniciar con la descripción de las funcionalidades del código, es importante pasar por la forma en la que se ha distribuído el mismo. El código fuente tanto del cliente (la plataforma web experimental) como del servidor (el servicio hecho en Go) se encuentran en un monorepo.

\subsection{\normalsize\normalfont\it Distribución del servidor}

\begin{center}
    \includegraphics[scale=0.45]{treeConvention.png}
\end{center}


Actualmente no existe una sola convención para la forma en la que se distribuyen los archivos de un proyecto hecho en go, sin embargo se suele utilizar la carpeta "cmd" para registrar los comandos principales y "pkg" para grupos de código (paquetes) que son útiles para el sistema pero no merecen su propio proyecto a parte.

Dentro de "pkg", los paquetes más usados y útiles son "wave", "rhythm" y "nxosc". Mientras que dentro de "nxosc" están las definiciones de los "procesadores de muestras", cada uno con su implementación para la interfaz asignada:

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
type SignalProc interface {
    Process(float64, float64, float64)  float64
}
\end{minted}

\bigskip

Primero la definición para la función de onda sinusoidal, esta toma una marca de tiempo como parámetro:

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
    type SINE struct {
        Volume  float64
    }
\end{minted}

\bigskip

A este tipo de oscilador le asignamos esta función:

\begin{equation*}
    f(x) = A\cdot\sin{2\pi(ft)}
\end{equation*}

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
    signal := amplitude * math.Sin(2 * math.Pi * angle)
\end{minted}

\bigskip

Similarmente, las funciones para la función de sierra y de pulso son, respectivamente:

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
    signal := amplitude * angle - math.Floor(angle)
    signal := amplitude * math.Sin(2 * math.Pi * angle))
\end{minted}

\bigskip

Cada una de estas funciones, como se puede notar, devuelven valores de punto flotante con una precisión de 64 bits (8 bytes), esto es beneficioso pues el rango superior le da mucha mayor precisión a la muestra y por ende mayor calidad de sonido final.

Luego, necesitamos una melodía que determine la secuencia de frecuencias que debe seguir el sintetizador, es acá donde definimos el paquete de ritmo.

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
type MelodyNode struct {
    id          string
    Frequency   float64
    Velocity    float64
    Duration    float64
    Next        []*MelodyNode
    Prev        *MelodyNode
}
\end{minted}

A diferencia de lenguajes como python o java, que procesan implícitamente las referencias a direcciones de memoria, tenemos que guardar explícitamente direcciones de memoria a nodos que a su vez apunten a más nodos, que a su vez apunten a más nodos y así.

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
type Melody struct {
    Head        *MelodyNode
}
\end{minted}

De este modo ya armamos la estructura de la maquina de estados de todas las posibles melodías, nos falta definir la forma en la que obtendremos tanto la duración de una melodía aleatoria, como la estructura de la melodía en sí.

\begin{minted}[fontsize=\scriptsize]{go}
func GetPath(melody Melody) (float64, []int) {
    source := rand.NewSource(time.Now().UnixNano())
    var path []int

    var sum float64 = 0.0
    var ranIndex int
    for a := melody.Head; a != nil; {
        sum += a.Duration
        ranIndex = ranChoice(a.Next, source)
        if ranIndex == -1 {
            return sum, path
        }

        path = append(path, ranIndex)
        a = a.Next[ranIndex]
    }

    return sum, path
}
\end{minted}

Acá estamos primero creando un nuevo generador de numeros pseudo-aleatorios que use como seed el tiempo actual según el estandar de UNIX y en nanosegundos, esto asegura un mucho mayor nivel de entropía para melodías lo suficientemente grandes.

Con cada salto que damos entre unidades de melodía, se guarda su índice de la lista de posibles hijos del hijo anterior, además de que se acumula su duración.

La melodía que se genera mas adelante será procesada por el sintetizador para producir un archivo de audio, pero, ¿Cómo se guardan los archivos de audio?

Primero, cada archivo ".wav" se compone de 2 piezas, el "header" de 44 bytes y la data en sí.

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
type wavHeader struct {
    ChunkId             [4]byte
    ChunkSize           uint32
    Format              [4]byte
    Subchunk1Id         [4]byte
    Subchunk1Size       uint32
    AudioFormat         uint16
    NumChannels         uint16
    SampleRate          uint32
    ByteRate            uint32
    BlockAlign          uint16
    BitsPerSample       uint16
    Subchunk2Id         [4]byte
    Subchunk2Size       uint32
}
\end{minted}

\bigskip

El header se compone de información que se completa en base al contenido, de esta información nos importan principalmente el número de canales, el tamaño del buffer de cada muestra, el sample rate y la duración

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
func NewWaveFile(
    channels uint16,
    bufferSize uint16,
    sampleRate uint32,
    dataDuration float64
) *WavFile
\end{minted}

\newpage

El tamaño del buffer indica que tan grandes son nuestras muestras de audio, en nuestro caso, como usamos el estándar para archivos ".wav" o "wavefiles", este tamaño es de 2 bytes, es decir 16 bits. Luego, el sample rate indica que tantas muestras hay en cada segundo de audio, por motivos de calidad se eligió el estándar usado para cds y dvds (48 KHz).

La forma en la que se analiza toda esta información se contiene en este proceso:

\bigskip

\begin{minted}[fontsize=\scriptsize]{go}
head := melody.Head
for pathIndex := 0; pathIndex <= len(path); pathIndex++ {
    var (
        decayRate float64=(start-end+1)/head.Duration
        timeStamp float64=0.0
        offset    uint32 =0
    )

    unitSamples := math.Floor(head.Duration * SampleRateVal)

    for ; offset < unitSamples; offset++ {
        timeStamp = float64(offset)/float64(SampleRateVal)
        floatingPointSample := sampleOsc.Process(
            timeStamp,
            head.Frequency,
            1.0,
        )*effects.ExpDecayFac(1.0,timeStamp,decayRate)*toPcmInt16

        binary.LittleEndian.PutUint16(
            dataWhole[currentSample + offset * uint32(bufferSize):],
            uint16(math.Floor(floatingPointSample)),
        )
    }

    currentSample += unitSamples
    if pathIndex < len(path) {
        head = head.Next[path[pathIndex]]
    }
}
\end{minted}

\bigskip

Primero se asigna un puntero "head" a la cabeza de toda la melodía, este proceso se deber repetir por cada unidad de ritmo dentro de la melodía cuyo índice haya sido especificado dentro del "path", calculado con el método "GetPath" mostrado anteriormente. Entonces, esto se puede leer como traversar la información que pertenece a los nodos elegidos aleatoriamente.

Para cada uno de estos nodos se obtienen todos los valores de las muestras de amplitud de cada nodo (cada nodo tiene su propia duración y frecuencia, como notas musicales). Cuando ya se tiene el valor procesado con el decay exponencial aplicado se agrega el valor a la lista de bytes de toda la data "dataWhole".

Esta data ya procesada es la que se escribe dentro de un archivo ".wav" que se devuelve al usuario a través de la petición web.

\subsection{\normalfont\small\it Cliente HTTP}

\begin{center}
    \includegraphics[scale=0.15]{uchain.png}
\end{center}

El cliente es un visualizador para la data que será enviada al servidor, básicamente permite manipular de forma interactiva la data antes de convertirla en música.

En el caso del ejemplo, la data tiene la siguiente forma:

\bigskip

\begin{minipage}{0.4\linewidth}
    \begin{center}
        \begin{minted}[fontsize=\tiny]{json}
{
    "nodes": [
        {
            "id": "1",
            "color": "white",
            "Frequency": 400,
            "Duration": 0.25
        },
        {
            "id": "2",
            "color": "red",
            "Frequency": 1000,
            "Duration": 0.25
        },
        {
            "id": "3",
            "color": "blue",
            "Frequency": 400,
            "Duration": 0.25
        },
        {
            "id": "4",
            "color": "red",
            "Frequency": 400,
            "Duration": 0.25
        },
        {
            "id": "5",
            "color": "red",
            "Frequency": 1000,
            "Duration": 0.25
        },
        {
            "id": "6",
            "color": "green",
            "Frequency": 400,
            "Duration": 0.25
        }
    ],
    "links": [
        {
            "source": "1",
            "target": "5"
        },
        {
            "source": "5",
            "target": "4"
        },
        {
            "source": "2",
            "target": "3"
        },
        {
            "source": "1",
            "target": "2"
        },
        {
            "source": "2",
            "target": "6"
        }
    ]
}
            \end{minted}
    \end{center}
\end{minipage}

\bigskip

De la información de cada nodo y sus conexiones, el servidor elabora el árbol de la melodía para que luego el cliente la reproduzca.

\newpage

\section{\normalsize\normalfont\centering Resultados y Discusión}

Debido a la naturaleza académica/recreativa del desarrollo del proyecto, no se pudo desarrollar una colecta real de datos para ser analizados, sin embargo, gracias a las fuentes estudiadas, se obtuvo un contexto lo suficientemente bueno de resultados previos de investigaciones como para simular datos.

Los resultados de la simulación y análisis de los datos sintéticos revelaron patrones significativos en la interacción de los usuarios con el sintetizador de audio basado en \textmu chain. Se observó una clara preferencia por determinados tipos de osciladores y combinaciones de efectos en función de la edad y las preferencias musicales simuladas de los perfiles hipotéticos.

Además, se identificaron tendencias en la configuración de parámetros, donde ciertas combinaciones de frecuencia, amplitud y volumen fueron más frecuentes entre los perfiles simulados de usuarios con características similares. Estos datos sugerían una mayor inclinación hacia la experimentación con sonidos más complejos entre los usuarios jóvenes, mientras que los perfiles simulados de usuarios de mayor edad tendían a preferir configuraciones más simples.

En relación con las preguntas iniciales de investigación, los resultados demostraron que la flexibilidad proporcionada por la recursividad en el diseño de µchain permitió una síntesis de sonido más adaptable y versátil. Además, el enfoque matemático cuantitativo utilizado en la simulación permitió una comprensión detallada de las preferencias y comportamientos de los perfiles simulados, proporcionando información valiosa sobre cómo los usuarios podrían interactuar con el sintetizador en un entorno real.

\section{\normalsize\normalfont\centering Conclusiones y Futuras Direcciones}

A manera de conclusión, los hallazgos obtenidos destacan la importancia de la modularidad y la recursividad en el diseño de sistemas de síntesis de audio como \textmu chain. Estos resultados ofrecen una comprensión profunda de las interacciones hipotéticas de los usuarios con el sintetizador y su comportamiento ante diversas configuraciones de parámetros. Este conocimiento tiene un impacto significativo en el desarrollo de software para la música y la producción de audio, ya que proporciona una guía sobre las preferencias potenciales de los usuarios y la adaptabilidad necesaria en este campo.

Se recomienda explorar aún más las interacciones entre los usuarios y el sintetizador mediante la simulación de escenarios más complejos y la integración de modelos de inteligencia artificial para predecir y adaptarse a las preferencias individuales en tiempo real. Además, se sugiere investigar cómo las tendencias identificadas en los perfiles simulados pueden aplicarse a la creación de interfaces de usuario más intuitivas y personalizadas en futuras versiones del software de síntesis de audio.

El estudio de simulación proporciona una base sólida para investigaciones futuras, incluyendo el análisis de la influencia de factores adicionales como la ubicación geográfica simulada, la cultura o el contexto social en las preferencias de los usuarios, lo que podría enriquecer aún más nuestra comprensión del comportamiento humano en la creación de música mediante herramientas digitales.

\section{\normalsize\normalfont\centering Referencias}

\begingroup
\renewcommand{\section}[2]{}
\footnotesize
\normalfont

\bibliographystyle{plain} % We choose the "plain" reference style
\bibliography{refs} % Entries are in the refs.bib file

\endgroup







\end{document}

