import * as d3 from 'd3'
import * as dataset from '../data/dataset.json'

// set the dimensions and margins of the graph
const margin = {
  top: 10,
  bottom: 30,
  right: 30,
  left: 40
}

const center = () => {
  return [
    window.innerWidth / 2,
    window.innerHeight / 2
  ]
}

const simulation = d3.forceSimulation()
  .force(
    "link",
    d3.forceLink().id(d => d.id)
  )
  .force("charge", d3.forceManyBody().strength(-1000)) // This adds repulsion (if it's negative) between nodes.
  .force("center", d3.forceCenter(...center())) // This force attracts nodes to the center of the svg area

window.addEventListener("resize", () => {
  simulation.force("center", d3.forceCenter(...center())) // This force attracts nodes to the center of the svg area
})

// append the svg object to the body of the page
const svg = d3.select('#my_dataviz')
  .append("svg")
  .attr("width", "100vw")
  .attr("height", "100vh")
  .append("g")
  .attr("transform", `translate(${margin.left},${margin.top})`)

// Initialize the links
const link = svg.append("g")
  .attr("class", "links")
  .style("stroke", "#E0E0E0")
  .selectAll("line")
  .data(dataset.links)
  .enter().append("line");

//When the drag gesture starts, the targeted node is fixed to the pointer
//The simulation is temporarily “heated” during interaction by setting the target alpha to a non-zero value.
const dragstarted = (event, d) => {
  if (!event.active) simulation.alphaTarget(0.3).restart();//sets the current target alpha to the specified number in the range [0,1].
  d.fy = d.y; //fx - the node’s fixed x-position. Original is null.
  d.fx = d.x; //fy - the node’s fixed y-position. Original is null.
}

//When the drag gesture starts, the targeted node is fixed to the pointer
const dragged = (event, d) => {
  d.fx = event.x;
  d.fy = event.y;
}

//the targeted node is released when the gesture ends
const dragended = (event, d) => {
  if (!event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

// This function is run at each iteration of the force algorithm, updating the nodes position (the nodes data array is directly manipulated).
function ticked() {
  link.attr("x1", d => d.source.x)
    .attr("y1", d => d.source.y)
    .attr("x2", d => d.target.x)
    .attr("y2", d => d.target.y);

  node.attr("cx", d => d.x)
    .attr("cy", d => d.y)

  text.attr("x", d => d.x) //position of the lower left point of the text
    .attr("y", d => d.y); //position of the lower left point of the text
}

// Initialize the nodes
const node = svg.append("g")
  .attr("class", "nodes")
  .selectAll("circle")
  .data(dataset.nodes)
  .enter().append("circle")
  .attr("r", 20)
  .call(
    d3.drag()  //sets the event listener for the specified typenames and returns the drag behavior.
    .on("start", dragstarted) //start - after a new pointer becomes active (on mousedown or touchstart).
    .on("drag", dragged)      //drag - after an active pointer moves (on mousemove or touchmove).
    .on("end", dragended)     //end - after an active pointer becomes inactive (on mouseup, touchend or touchcancel).
  )
  .attr("fill", d => d.color)

const text = svg.append("g")
  .attr("class", "text")
  .selectAll("text")
  .data(dataset.nodes)
  .enter().append("text")
  .text((d) => {
    return d.id
  })


//Listen for tick events to render the nodes as they update in your Canvas or SVG.
simulation
  .nodes(dataset.nodes)//sets the simulation’s nodes to the specified array of objects, initializing their positions and velocities, and then re-initializes any bound forces;
  .on("tick", ticked);//use simulation.on to listen for tick events as the simulation runs.
// After this, Each node must be an object. The following properties are assigned by the simulation:
// index - the node’s zero-based index into nodes
// x - the node’s current x-position
// y - the node’s current y-position
// vx - the node’s current x-velocity
// vy - the node’s current y-velocity

simulation.force("link")
  .links(dataset.links);//sets the array of links associated with this force, recomputes the distance and strength parameters for each link, and returns this force.
// After this, Each link is an object with the following properties:
// source - the link’s source node
// target - the link’s target node
// index - the zero-based index into links, assigned by this method


//===========================================

const choice = (arr) => {
  const index = Math.floor(Math.random() * arr.length)
  return index
}

const nodesFreqs = []
dataset.nodes.forEach((node) => {
  nodesFreqs.push({
    id: node.id,
    freq: node.Frequency
  })
})

const linksRels = []
dataset.nodes.forEach((node) => {
  const connections = dataset.links.map((link) => {
    if (link.source.id === node.id) {
      return link.target.id
    }
  }).filter(d => !(d === undefined))

  linksRels.push({
    id: node.id,
    conns: connections
  })
})


const obj = {
  links: linksRels
}

const nameobj = await fetch("http://localhost:6969/api", {
  method: "POST",
  body: JSON.stringify(obj)
}).then(console.log)











