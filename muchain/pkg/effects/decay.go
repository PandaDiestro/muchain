package effects

import "math"

func ExpDecayFac(initial float64, timeStamp float64, rate float64) float64 {
    return initial * float64(math.Exp(-1.0 * timeStamp * rate))
}



