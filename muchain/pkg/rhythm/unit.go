package rhythm

import (
	"errors"
	"math/rand"
	"time"
)

type Unit struct {
    Frequency   float64     `json:"freq"`
    Velocity    float64     `json:"vel"`
    Duration    float64     `json:"duration"`
}

type Link struct {
    Id          string      `json:"id"`
    Connections []string    `json:"conns"`
    Data        Unit        `json:"data"`
}

type Request struct {
    Links       []Link      `json:"links"`
}

type MelodyNode struct {
    id          string
    Frequency   float64
    Velocity    float64
    Duration    float64
    Next        []*MelodyNode
    Prev        *MelodyNode
}

type Melody struct {
    Head        *MelodyNode
}

func whereInList(i string, list []MelodyNode) (error, int) {
    length := len(list)
    if length == 0 {
        return errors.New("Empty list"), -1
    }

    for index := range list {
        if i == list[index].id {
            return nil, index
        }
    }

    return errors.New("Element is not in list"), -1
}

func ParseRequest(req *Request) Melody {
    var returnable Melody
    nodes := make([]MelodyNode, len(req.Links))

    for index := range req.Links {
        nodes[index] = MelodyNode{
            id: req.Links[index].Id,
            Frequency: req.Links[index].Data.Frequency,
            Duration: req.Links[index].Data.Duration,
            Velocity: req.Links[index].Data.Velocity,
        }
    }

    for index := range nodes {
        for jndex := range req.Links[index].Connections {
            listErr, position := whereInList(req.Links[index].Connections[jndex], nodes)
            if listErr != nil {
                panic(listErr.Error())
            }

            nodes[position].Prev = &nodes[index]
            nodes[index].Next = append(nodes[index].Next, &nodes[position])
        }
    }

    returnable.Head = &nodes[0]
    for returnable.Head.Prev != nil {
        returnable.Head = returnable.Head.Prev
    }

    return returnable
}

func ranChoice(list []*MelodyNode, source rand.Source) int {
    if len(list) == 0 {
        return -1
    }

    return int(source.Int63()) % len(list)
}

func GetPath(melody Melody) (float64, []int) {
    source := rand.NewSource(time.Now().UnixNano())
    var path []int

    var sum float64 = 0.0
    var ranIndex int
    for a := melody.Head; a != nil; {
        sum += a.Duration
        ranIndex = ranChoice(a.Next, source)
        if ranIndex == -1 {
            return sum, path
        }

        path = append(path, ranIndex)
        a = a.Next[ranIndex]
    }

    return sum, path
}

