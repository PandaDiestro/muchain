package nxosc

import (
    "math"
)

type SignalProc interface {
    Process(float64, float64, float64)  float64
}

type SAW struct {
    Volume  float64
}

func (proc *SAW) Process(timeStamp float64, frequency float64, amplitude float64) float64 {
    var angle float64 = timeStamp * frequency
    signal := amplitude * float64(angle - float64(math.Floor(float64(angle))))
    return signal * proc.Volume * 0.01
}

type SINE struct {
    Volume  float64
}

func (proc *SINE) Process(timeStamp float64, frequency float64, amplitude float64) float64 {
    var angle float64 = timeStamp * frequency
    signal := amplitude * float64(math.Sin(2 * math.Pi * float64(angle)))
    return signal * proc.Volume * 0.01
}

type SQUARE struct {
    Volume  float64
}

func (proc *SQUARE) Process(timeStamp float64, frequency float64, amplitude float64) float64 {
    var angle = timeStamp * frequency
    var angleFloorLeft  float64 = 2.0 * float64(math.Floor(float64(angle)))
    var angleFloorRight float64 = float64(math.Floor(2.0 * float64(angle)))

    signal := amplitude * 2 * (angleFloorLeft - angleFloorRight) + amplitude
    return signal * proc.Volume * 0.01
}

type NxOsc struct {
    Oscillators     []SignalProc
    Channels        uint16
}

func (proc NxOsc) Process(timeStamp float64, frequency float64, amplitude float64) float64 {
    var signal float64
    for _, osc := range proc.Oscillators {
        signal += osc.Process(timeStamp, frequency, amplitude)
    }

    return (signal / float64(len(proc.Oscillators))) / float64(proc.Channels)
}


