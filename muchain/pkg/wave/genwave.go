package wave

import (
	"math"
	"os"
	"unsafe"
)

type wavHeader struct {
    ChunkId             [4]byte
    ChunkSize           uint32
    Format              [4]byte
    Subchunk1Id         [4]byte
    Subchunk1Size       uint32
    AudioFormat         uint16
    NumChannels         uint16
    SampleRate          uint32
    ByteRate            uint32
    BlockAlign          uint16
    BitsPerSample       uint16
    Subchunk2Id         [4]byte
    Subchunk2Size       uint32
}


func (head *wavHeader) bytes() []byte {
    const sz = int(unsafe.Sizeof(wavHeader{}))
    return (*(*[sz]byte)(unsafe.Pointer(head)))[:]
}

type WavFile struct {
    header              *wavHeader
    data                []byte
}

func NewWaveFile(channels uint16, bufferSize uint16, sampleRate uint32, dataDuration float64) *WavFile {
    sampleNum := uint32(math.Floor((float64(sampleRate) * float64(dataDuration))))
    newHeader := &wavHeader{
        ChunkId: [4]byte{'R', 'I', 'F', 'F'},
        ChunkSize: 36 +  sampleNum * uint32(channels * bufferSize),
        Format: [4]byte{'W', 'A', 'V', 'E'},
        Subchunk1Id: [4]byte{'f', 'm', 't', 0x20},
        Subchunk1Size: 16,
        AudioFormat: 1,
        NumChannels: channels,
        SampleRate: sampleRate,
        ByteRate: sampleRate * uint32(channels * bufferSize),
        BlockAlign: channels * bufferSize,
        BitsPerSample: bufferSize * 8,
        Subchunk2Id: [4]byte{'d', 'a', 't', 'a'},
        Subchunk2Size: sampleNum * uint32(channels * bufferSize),
    }

    return &WavFile{
        header: newHeader,
        data: nil,
    }
}

func (file *WavFile) SetData(data []byte) {
    file.data = data
}

func (file *WavFile) Write() error {
    const outName string = "out.wav"
    outFile, createErr := os.Create(outName)
    if createErr != nil {
        return createErr
    }

    var writeErr error
    _, writeErr = outFile.Write(file.header.bytes())
    if writeErr != nil {
        return writeErr
    }

    _, writeErr = outFile.Write(file.data)
    if writeErr != nil {
        return writeErr
    }

    return nil
}



