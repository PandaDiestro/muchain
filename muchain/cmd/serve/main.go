package main

import (
	"encoding/binary"
	"encoding/json"
	"log"
	"math"
	"net/http"

	"muchain/pkg/effects"
	"muchain/pkg/nxosc"
	"muchain/pkg/rhythm"
	"muchain/pkg/wave"
)

const (
    SampleRateVal   uint32  = 48000
    start           float64 = 1.0
    end             float64 = 0.0
    bufferSize      uint16  = 2
    channels        uint16  = 1
)

const toPcmInt64 float64 = 9223372036854775808.0
const toPcmInt16 float64 = 32768.0

func CorsMiddleware(handle http.HandlerFunc, methods string) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request){
        w.Header().Set("Content-Type", "application/json; charset=utf-8")
        w.Header().Set("Access-Control-Allow-Methods", methods)
        w.Header().Set("Access-Control-Allow-Origin", "*")
        w.Header().Set("Access-Control-Allow-Headers", "*")

        if r.Method == http.MethodOptions {
            w.WriteHeader(http.StatusOK)
            return
        }

        handle.ServeHTTP(w, r)
    }
}

func fillData(w http.ResponseWriter, r *http.Request) {
    var ClientData rhythm.Request
    decodeErr := json.NewDecoder(r.Body).Decode(&ClientData)
    if decodeErr != nil {
        log.Println(decodeErr)
        return
    }

    log.Println(ClientData)

    melody := rhythm.ParseRequest(&ClientData)
    totalLen, path := rhythm.GetPath(melody)

    var (
        melodySamples   uint32  = uint32(math.Floor(float64(SampleRateVal) * totalLen))
        dataWhole       []byte  = make([]byte, melodySamples * uint32(bufferSize))
    )

    wave := wave.NewWaveFile(
        1,          // channels
        2,          // buffer size (in bytes)
        48000,      // sample rate
        totalLen,   // duration (in seconds)
    )

    sampleOsc := nxosc.NxOsc {
        Channels: channels,
        Oscillators: []nxosc.SignalProc {
            &nxosc.SINE {
                Volume: 100,
            },
            &nxosc.SAW {
                Volume: 100,
            },
            &nxosc.SQUARE {
                Volume: 100,
            },
        },
    }

    var currentSample uint32 = 0

    head := melody.Head
    log.Println(path)
    for pathIndex := 0; pathIndex <= len(path); pathIndex++ {
        var (
            decayRate       float64 = (start - end + 1) / head.Duration
            timeStamp       float64 = 0.0
            offset          uint32  = 0
            unitSamples     uint32  = uint32(math.Floor(head.Duration * float64(SampleRateVal)))
        )

        for ; offset < unitSamples; offset++ {
            timeStamp = float64(offset) / float64(SampleRateVal)
            floatingPointSample := sampleOsc.Process(
                timeStamp,
                head.Frequency,
                1.0,
            ) * effects.ExpDecayFac(1.0, timeStamp, decayRate) * toPcmInt16

            binary.LittleEndian.PutUint16(
                dataWhole[currentSample + offset * uint32(bufferSize):],
                uint16(math.Floor(floatingPointSample)),
            )
        }

        currentSample += unitSamples
        if pathIndex < len(path) {
            head = head.Next[path[pathIndex]]
        }
    }

    wave.SetData(dataWhole)
    wave.Write()

    w.Write([]byte("{\"filename\": \"out.wav\"}"))
    return
}

func fileData(w http.ResponseWriter, r *http.Request) {
    urlParams := r.URL.Query()
    http.ServeFile(w, r, urlParams["name"][0])
    return
}


func main() {
    http.HandleFunc("/api", CorsMiddleware(fillData, "POST"))
    http.HandleFunc("/file", fileData)

    log.Println("Listening @6969")

    http.ListenAndServe(":6969", nil)
}

