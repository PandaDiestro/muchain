# μchain

![](./doc/media/banner.png)

μchain, pronounced /mju/chain, is an experimental musical platform based on information trees with samples and effects composed of data inside nodes which will are used to "render" music.

*"We may be working in a similar way, and all be making recordings based on static walls of noise, but the final results depend on the individual personalities of the individuals concerned."*

-Vomir, Harsh Noise Wall Superstar.

## The *imp* music format

This platform is powered by the '\*.imp' music format (similar to MIDI, except for the fact that its specification isn't that much versatile *pardon me, given the deadline I was given for a working prototype of this platform, I could not come up with a better one*).

Each note can be thought as a set of Frequency, Amplitude and Duration ${f, A, D}$ where $\{f; A; D\} \in f64$ and $f, A, D \geq 0$. Considering $f64$ as the set of all 64 bit little-endian signed floats.






